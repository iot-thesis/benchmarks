const benchmarker = require('./MemoryProfiler');

/*
 * Example runs:
 *  node --allow-natives-syntax --expose-gc MemoryBenchmark.js SECRO 1 100 ./out/secroRun.txt
 *  node --allow-natives-syntax --expose-gc MemoryBenchmark.js TREE 1 100 ./out/treeRun.txt
 *  node --allow-natives-syntax --expose-gc MemoryBenchmark.js CjRDT 1000 ./out/cjrdtRun.txt
 */

/*
 * Benchmarks the collaborative text editor.
 * Benchmarks the time it takes to execute `insertAfter` operations locally,
 * in function of the operation history's size.
 *
 * Expected CLI arguments:
 *  - version to run: JSON CRDT (CjRDT) or SECRO or tree-based SECRO (TREE)
 *  - [commit interval] (if version = SECRO or TREE)
 *  - number of operations to execute
 *  - file to write measurements to
 */

global.version = process.argv[2].toUpperCase();
var TextEditorService, commitInterval, maxOps, file, Character;

switch (version) {
    case 'SECRO':
    case 'TREE':
        const te          = require('../CTE-Service');
        TextEditorService = te.TextEditorService;
        commitInterval    = parseInt(process.argv[3]);
        maxOps            = parseInt(process.argv[4]);
        file              = process.argv[5];
        if (version === 'SECRO') {
            Character = te.Character;
        }
        break;
    case 'CJRDT':
        TextEditorService = require('../CTE-CjRDT').TextEditorService;
        maxOps            = parseInt(process.argv[3]);
        file              = process.argv[4];
        commitInterval    = maxOps + 10; // such that we do not attempt to commit
        break;
    default:
        throw new Error('Unrecognized version.');
};

// Avoid compiler (JIT) optimizations
%NeverOptimizeFunction(insertOrDelete);
%NeverOptimizeFunction(insert);
%NeverOptimizeFunction(del);

benchmarker.setSetup(() => {
    editor = (new TextEditorService()).textEditor;
    ctr = 0;
    docSize = 0;
    previousChar = null;

    // Garbage collect right before starting the benchmark
    forceGC();
});

benchmarker.setFn(insertOrDelete.bind(null, 'a'));

benchmarker.setAfterFn(() => {
    if (ctr !== 0 && ctr % commitInterval === 0)
        editor.commit();
    ctr++;

    // Force GC after every operation,
    // to get the actual memory usage of every operation.
    forceGC();
});

const xRange = Array.from(new Array(maxOps), (x,i) => i+1); // array is the range: [0, 1, 2, ..., maxOps]
benchmarker.run(xRange, file); // run the benchmark for 1 to 1000 operations in the history

// Appends the character to the text document
function insert(char) {
    switch (version) {
        case 'CJRDT':
            editor.insertAfter(docSize, char);
            break;
        case 'SECRO':
            previousChar = editor.insertAfter(previousChar, new Character(char));
            break;
        case 'TREE':
            previousChar = editor.insertAfter(previousChar, char);
            break;
    }

    docSize++;
}

function del() {
    editor.delete(0);
    docSize--;
}

function insertOrDelete(char, idx) {
    // Always perform 100 insertions followed by 100 deletions
    // such that the memory usage grows and shrinks
    const div = Math.floor(ctr / 100);
    if (div % 2 === 0) {
        // Even multiple of 100
        insert(char);
        return "insert";
    }
    else {
        // Odd multiple of 100
        del();
        return "delete";
    }
}

// Forces garbage collection
function forceGC() {
    if (global.gc)
        global.gc();
    else
        console.log('Garbage collection unavailable. Pass --expose-gc when launching node to enable forced garbage collection.');
}