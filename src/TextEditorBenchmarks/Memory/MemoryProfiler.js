const writeToFile = require('../../FileWriter');
const { performance, PerformanceObserver, constants } = require('perf_hooks');

/*
 * Disable JIT compilation for these functions.
 * Requires running node with the --allow-natives-syntax flag.
 */
%NeverOptimizeFunction(bench);
%NeverOptimizeFunction(time);
%NeverOptimizeFunction(run);

var fn, setup, afterFn;

// Sets the function to benchmark
function setFn(timedFn) {
    fn = timedFn;
    //%NeverOptimizeFunction(fn);
}

// Sets th afterFn function which is called after every execution of `fn`.
function setAfterFn(fn) {
    afterFn = fn;
}

function setSetup(fun) {
    setup = fun;
}

function run(xs, file) {
    /*
     * Perform some warmup rounds.
     * Notice that these warmup rounds only works if we write them manually,
     * i suspect that a for loop gets optimized in some way.
     */
    console.log("calling setup");
    setup();
    console.log("WARMUP 1");
    bench(xs);
    setup();

    // Run the actual benchmark after a delay of 50ms
    // In order to allow the GC to kick in before we start.
    setTimeout(() => {
        bench(xs, true).then(samples => {
            // Write time measurements to file
            const header = "x\tDuration\tRSS\tHeapTotal\tHeapUsed\tExternal\tResult\n";
            var data = "";
            samples.forEach(sample => {
                const [x, duration, {rss, heapTotal, heapUsed, external}, res] = sample;
                data += `${x}\t${duration}\t${rss}\t${heapTotal}\t${heapUsed}\t${external}\t${res}\n`;
            });

            console.log("Going to write to " + file);
            writeToFile(data, file, process.exit, header);
        });
    }, 50);
}

/*
 * Benchmarks the execution time of a given function for various values of x.
 * i.e. measures the dependent variable y in function of the the independent variable x.
 * Returns an array of samples, where a sample is an array containing x and y: [x, y] (y = measured execution time)
 */
function bench(xs, observe) { //, wr) {
    const samples = [];

    // Store initial memory usage
    const initialSample = [xs[0]-1, 'NA', process.memoryUsage(), 'NA'];

    // Proceed with the actual benchmark
    if (!observe) {
        // Synchronous execution
        for (var x of xs) {
            const {result, duration} = time(fn.bind(null, x), `start:${x}`);
            const memUsage = process.memoryUsage();
            samples.push([x, duration, memUsage, result]);
            afterFn();
        }

        return [initialSample, ...samples];
    }
    else {
        // Asynchronous execution

        // Runs the first element in xs, and waits a few seconds before proceeding with the next.
        // Waiting a small time is needed in order to let the GC kick in.
        function run(xs, resolve) {
            if (xs.length > 0) {
                const x = xs[0];
                const {result, duration} = time(fn.bind(null, x), `start:${x}`);
                const memUsage = process.memoryUsage();
                samples.push([x, duration, memUsage, result]);
                afterFn();

                xs.splice(0, 1); // remove first element
                setTimeout(run.bind(null, xs, resolve), 50);
            }
            else {
                // Resolve the promise with the actual end result
                // Again, only after a small delay to allow for any eventual GC to still kick in
                setTimeout(() => {
                    resolve([initialSample, ...samples]);
                }, 50);
            }
        }

        return new Promise(resolve => run(xs, resolve));
    }
}

// Returns the time to execute function `fn` as well as the result
function time(fn, startMark) {
    performance.mark(`start`); // Start time //

    const res = fn();

    performance.mark(`end`);
    performance.measure(`duration`, `start`, `end`); // time it took to compute 10K tangents

    const perfEntries = performance.getEntriesByName(`duration`); // array of PerformanceEntry objects
    var duration = perfEntries[0].duration;

    // Correct duration if garbage collection was triggered
    const gcEntries = performance
        .getEntriesByType('gc')
        // Only retain entries that happened between both markers
        .filter(entry => entry.start >= perfEntries[0].start && entry.start <= perfEntries[0].end);

    // Remove GC durations from `duration`
    gcEntries.forEach(entry => {
        // Only retain entries that happened between both markers
        var gcEnd = gcStart + entry.duration,
            gcDuration = entry.duration;

        if (gcEnd > perfEntries[0].end) {
            console.log("Unexpected");
            gcDuration -= gcEnd - perfEntries[0].end;
        }

        GCs++;
        console.log(`GC duration: ${gcDuration} ms`);
        duration -= gcDuration;
    });

    // Clear the performance timeline
    performance.clearMarks('start');
    performance.clearMarks('end');
    performance.clearMeasures();
    performance.clearGC();

    return { result: res, duration: duration };
}

/*
 * Returns the kind of GC the performance entry represents.
 * Can be one of: MAJOR, MINOR, INCREMENTAL or WEAKCB.
 */
function gcKind(entry) {
    switch (entry.kind) {
        case constants.NODE_PERFORMANCE_GC_MAJOR:
            return "MAJOR";
        case constants.NODE_PERFORMANCE_GC_MINOR:
            return "MINOR";
        case constants.NODE_PERFORMANCE_GC_INCREMENTAL:
            return "INCREMENTAL";
        case constants.NODE_PERFORMANCE_GC_WEAKCB:
            return "WEAKCB";
        default:
            return "UNKNOWN";
    }
}

module.exports.setFn = setFn;
module.exports.run = run;
module.exports.setSetup = setSetup;
module.exports.setAfterFn = setAfterFn;