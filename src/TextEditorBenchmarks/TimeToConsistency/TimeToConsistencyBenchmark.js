const myIP = require('ip').address();
const writeToFile = require('../../FileWriter');

/*
 * Example runs:
 *  node --allow-natives-syntax --expose-gc TimeToConsistencyBenchmark.js SECRO 192.168.0.177 20 100 1 ./out/secroTTCrun.txt
 *  node --allow-natives-syntax --expose-gc TimeToConsistencyBenchmark.js TREE 192.168.0.177 20 100 1 ./out/treeTTCrun.txt
 *  node --allow-natives-syntax --expose-gc TimeToConsistencyBenchmark.js CjRDT 192.168.0.177 100 1 ./out/cjrdtTTCrun.txt
 */

/*
 * Benchmark on the collaborative text editor.
 * Measures the time it takes to reach consistency across all nodes.
 *
 * The benchmark is guided by a master node, which creates the service
 * and decides on the number of operations each node has to perform.
 * Each peer measures how much time elapsed until all operations were received and executed locally.
 *
 * Each peer then updates the replica to incorporate this time.
 * When the master node observes each node's time to consistency in the replica,
 * he writes those times to a file, marking the end of the benchmark.
 */

/* We expect the following CLI arguments:
 *  - the version to benchmark: CjRDT (JSON CRDT) or SECRO or TREE (tree-based SECRO text editor)
 *  - ip of the master node
 *  - [Commit interval] (only if version is SECRO or TREE)
 *  - total number of operations to execute
 *  - amount of nodes participating in the benchmark
 *  - File to write the benchmarking results to
 *
 *  Note that the total number of operations to execute must be divisable by the number of nodes.
 */

/*
 * Import the correct version
 */
global.version = process.argv[2].toUpperCase();

/*
 * If we are the master node, then we create and publish the service.
 * If we aren't, then we subscribe to the service.
 */

const master = process.argv[3]; // ip of the master node
var n, noNodes, resFile, commitInterval;
const _EXIT_TIME_ = 1000;

switch (version) {
    case 'SECRO':
    case 'TREE':
        if (process.argv.length !== 8)
            throw new Error(`Expected 6 arguments but got ${process.argv.length - 2}.`);
        commitInterval = parseInt(process.argv[4]);
        n              = parseInt(process.argv[5]); // total number of operations to execute
        noNodes        = parseInt(process.argv[6]); // number of nodes participating in the benchmark
        resFile        = process.argv[7];           // File to write benchmarking results to
        break;
    case 'CJRDT':
        if (process.argv.length !== 7)
            throw new Error(`Expected 5 arguments but got ${process.argv.length - 2}.`);
        n              = parseInt(process.argv[4]); // total number of operations to execute
        noNodes        = parseInt(process.argv[5]); // number of nodes participating in the benchmark
        resFile        = process.argv[6];           // File to write benchmarking results to
        commitInterval = n + 10;                    // Such that we do not attempt to commit
        break;
    default:
        throw new Error('Unrecognized version.');
}

var opCtr = 0;

if (n % noNodes !== 0)
    throw new Error(`Total number of operations (${n}) must be divisable by number of nodes (${noNodes}).`);

/*
 * An eventually consistent replica to coordinate
 * the peers that participate in the benchmark.
 */
class Synchronizer {
    constructor(count, peers = 0, status = "busy") {
        this.peers = count;
        this.endedPeers = peers; // set of ip addresses of peers that executed all their operations
        this.status = status;
    }

    // commutative increment operation
    increment() {
        this.peers += 1; // increment peer count
    }

    getPeerCount() {
        return this.peers;
    }

    endedOps() {
        this.endedPeers += 1;
    }

    getEnd() {
        return this.endedPeers === noNodes;
    }

    stop() {
        this.status = "done";
        // Give the peers some time to see this new status, then exit
        setTimeout(process.exit, _EXIT_TIME_);
    }

    getStatus() {
        return this.status;
    }

    tojson() {
        return { peers: this.peers, endedPeers: this.endedPeers, status: this.status };
    }

    static fromjson(info) {
        const {peers, endedPeers, status} = info;
        return new Synchronizer(peers, endedPeers, status);
    }
}

Synchronizer.accessors = new Set(['getPeerCount', 'getEnd', 'getStatus']);

var s = myIP === master ? createService() : getService(); // promise that resolves to the benchmarking service
var added = false, started = false, ended = false;

s.then(service => {
    var startTime;

    // Add a listener on the text editor
    service.textEditor.onUpdate(() => {
        if (service.textEditor.getSize() === (service.noOps * noNodes) && !added) {
            notifyEnd();
        }

        if (version === 'CJRDT' && myIP === master) {
            // Check if we got all samples
            if (service.samples.size() === noNodes && !ended) {
                ended = true; // to avoid writing the data multiple times
                // We got the time to consistency of every node
                console.log("*** END OF BENCHMARK ***");
                console.log("TTCs are: ");
                const ttcs = service.samples.getSamples();
                var times = [];
                for (var ip in ttcs) {
                    if (ttcs.hasOwnProperty(ip)) {
                        console.log(`${ip}: ${ttcs[ip]} ms`);
                        times.push(ttcs[ip]);
                    }
                }

                // Write maximum time to consistency to a file
                const header = "Version\tTotalOps\tTime\tNodes\tCommitInterval\n",
                      data   = `${version}\t${n}\t${Math.max(...times)}\t${noNodes}\t${commitInterval}\n`;
                writeToFile(data, resFile, service.synchronizer.stop, header);
            }
        }
    });

    // Use the synchronizer to wait until all peers discovered the service
    service.synchronizer.onUpdate(() => {
        if (service.synchronizer.getPeerCount() === noNodes && !started) {
            started = true;
            // All peers participating in the benchmark discovered the service
            // Start the benchmark
            console.log("*** Starting benchmark ***");
            console.log(Date.now());
            startTime = Date.now();
            benchmark(service);
        }

        // Checks if all peers executed all operations
        // Every peer executes `endOps` after he executed all his operations.
        // Since the order of messages is guaranteed, all operations were processed
        // when all ip addresses are in `service.synchronizer.endedPeers`
        // Note: we cannot rely on the length of the document because
        // operations concurrent with commit might be dropped.
        if (service.synchronizer.getEnd() && !added) {
            notifyEnd();
        }

        if (service.synchronizer.getStatus() === 'done' && myIP !== master) {
            // Got end signal
            process.exit();
        }
    });

    service.synchronizer.increment(); // increment the peer count since we discovered the service

    function notifyEnd() {
        added = true; // to avoid adding our ttc multiple times
        // Received and executed all operations,
        // add time to consistency to the service
        const endTime = Date.now(),
              ttc = endTime - startTime;

        console.log(`# executed operations: ` + service.textEditor.getSize());

        if (((version === 'SECRO' || version === 'TREE') && myIP === master) || version === 'CJRDT') {
            service.samples.addSample(myIP, ttc);
        }
        else {
            service.samples.then(s => s.addSample(myIP, ttc));
        }
    }
});

function benchmark(service) {
    // Execute operations
    var previousCharacter = null;
    for (var i = 0; i < service.noOps; i++) {
        previous = service.insertAfter(previousCharacter, 'a');
        opCtr++;
        if (opCtr % commitInterval === 0)
            service.textEditor.commit();
    }

    // Mark end of the operations
    // This operation will be broadcasted but is guaranteed to arrive after the previous operations.
    service.synchronizer.endedOps();
}

function createService() {
    if (version === 'SECRO' || version === 'TREE') {
        const {Character, _, TextEditor} = require('../CTE-Service');
        global.Char = Character;

        // `Service`, `Factory`, etc. are in the global scope
        // since we imported the text editor which on its turn imports the CScript DSL.
        Factory.registerCmRDTClass(Synchronizer);

        class BenchmarkService extends Service {
            constructor(noOps) {
                super();

                this.noOps = noOps;

                // Eventually consistent text editor
                this.textEditor = super.makeReplica('textEditor', new TextEditor());

                var that = this;

                // Eventually consistent object storing a counter of the number of discovered peers.
                // Peers that discover this service will increment this count.
                this.synchronizer = super.makeReplica('synchronizer', new Synchronizer(0));

                // Strongly consistent object maintaining the benchmark's results
                this.samples = super.makeReplica('samples', {
                    ttc: new Map(), // DCT of node ip address to its time to consistency
                    addSample: function(ip, time) {
                        this.ttc.set(ip, time);
                        if (this.ttc.size === noNodes) {
                            // We got the time to consistency of every node
                            // Write those times to a file
                            const header = "Version\tTotalOps\tTime\tNodes\tCommitInterval\n",
                                  data   = `${version}\t${n}\t${Math.max(...this.ttc.values())}\t${noNodes}\t${commitInterval}\n`;
                            writeToFile(data, resFile, () => that.synchronizer.stop(), header);

                            console.log("*** END OF BENCHMARK ***");
                            console.log("TTCs are: ");
                            this.ttc.forEach((time, ip) => console.log(`${ip}: ${time} ms`));
                            console.log(`Document's end state: ${that.textEditor.getContent()}`);
                            console.log("************************");
                        }
                    }
                });
            }

            insertAfter(prev, char) {
                if (version === "SECRO")
                    char = new Char(char);
                const res = this.textEditor.insertAfter(prev, char);
                return res;
            }
        }

        const bs = new BenchmarkService(n / noNodes);
        bs.publish('TTC_Benchmark');
        return new Promise(resolve => resolve(bs));
    }
    else if (version === 'CJRDT') {
        const stub = createServiceStub();
        return new Promise(resolve => resolve(stub));
    }
}

function getService() {
    return new Promise(resolve => {
        if (version === 'SECRO' || version === 'TREE') {
            const {Character, _, TextEditor} = require('../CTE-Service');
            global.Char = Character;

            Factory.registerCmRDTClass(Synchronizer);

            // Subscribe to this benchmark service
            Service.subscribe('TTC_Benchmark', bs => resolve(bs));
        }
        else if (version === 'CJRDT') {
            const stub = createServiceStub();
            resolve(stub);
        }
    });
}

function createServiceStub() {
    const cjrdt = require('cjrdt').default;

    cjrdt.doc = {
        endedPeers: [],
        status: 'busy',
        peers: [], // we use a list because concurrent assignments to primitives are not solved
        content: [],
        noOps: n / noNodes,
        samples: {}
    };

    return {
        noOps: cjrdt.doc.noOps.values().values().next().value,
        textEditor: {
            insertAfter: function(idx, char) {
                cjrdt.doc.content.idx(idx).insertAfter(char);
                return idx+1; // return index of the newly inserted character
            },
            getContent: function() {
                //console.log(cjrdt.doc.content);
                return cjrdt.doc.content.toJS();
            },
            getSize: function() {
                return cjrdt.doc.content.size();
            },
            onUpdate: function(fn) {
                cjrdt.onUpdate(fn);
            }
        },
        insertAfter: function(idx, char) {
            if (idx === null)
                idx = 0;
            return this.textEditor.insertAfter(idx, char);
        },
        synchronizer: {
            increment: function() { cjrdt.doc.peers.idx(0).insertAfter(myIP); },
            getPeerCount: function() { return cjrdt.doc.peers.size(); },
            onUpdate: function(fn) {
                cjrdt.onUpdate(fn);
            },
            endedOps: function() {
                cjrdt.doc.endedPeers.idx(0).insertAfter(myIP);
            },
            getEnd: function() {
                return cjrdt.doc.endedPeers.size() === noNodes;
            },
            stop: function() {
                cjrdt.doc.status = 'done';
                // Give the peers some time to see this new status, then exit
                setTimeout(process.exit, _EXIT_TIME_);
            },
            getStatus() {
                return cjrdt.doc.status.toJS();
            }
        },
        samples: {
            addSample: function(ip, time) {
                cjrdt.doc.samples[ip] = time;
                console.log(cjrdt.doc.samples.mapSize());
            },
            size: function() {
                return cjrdt.doc.samples.mapSize();
            },
            getSamples: function() {
                return cjrdt.doc.samples.toJS();
            }
        }
    };
}