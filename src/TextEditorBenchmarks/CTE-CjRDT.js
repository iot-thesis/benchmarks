const cte = require('CjRDT-CTE'); // local npm module, modify path to your needs in package.json!

class TextEditorService {
    constructor() {
        this.textEditor = new cte.TextEditor();
    }
}

module.exports.TextEditorService = TextEditorService;
module.exports.TextEditor        = cte.TextEditor;