/*
 * Relies on a global variable `version`,
 * to determine which version to load.
 * This can be either the naive SECRO version
 * or the tree-based SECRO version.
 */

var cte;
switch (version) {
    case 'SECRO':
        cte = require('SECRO-CTE'); // local npm module, modify path to your needs in package.json!
        break;
    case 'TREE':
        cte = require('TREE-CTE'); // local npm module, modify path to your needs in package.json!
        break;
    default:
        throw new Error('Unrecognized version.');
}

// `Service`, `Factory`, etc are in the global scope,
// due to the import of the collaborative text editor which imports the CScript DSL.

class TextEditorService extends Service {
    constructor() {
        super();
        this.textEditor = super.makeReplica('textEditor', new cte.TextEditor());
    }
}

module.exports.Character         = cte.Character;
module.exports.TextEditorService = TextEditorService;
module.exports.TextEditor        = cte.TextEditor;