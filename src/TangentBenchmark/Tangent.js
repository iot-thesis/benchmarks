const computeTans = require('./Computer');

class Tangent {
    constructor() {
        this.res = 0;
    }

    compute() {
        this.res = computeTans();
    }

    preCompute() {
        return true;
    }

    postCompute() {
        return true;
    }

    getRes() {
        return this.res;
    }

    tojson() {
        return this.res;
    }

    static fromjson(res) {
        const t = new Tangent();
        t.res = res;
        return t;
    }
}

Tangent.accessors = new Set(['getRes']);

module.exports = Tangent;