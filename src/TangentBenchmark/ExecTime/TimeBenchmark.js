const benchmarker = require('../../Timing');

/*
 * Example runs:
 *  node --allow-natives-syntax TimeBenchmark.js SECRO 20 100 ./out/run.txt
 *  node --allow-natives-syntax TimeBenchmark.js CJRDT 1000 ./out/runCjRDT.txt
 */

/*
 * Benchmarks the tangent service.
 * Expects the following CLI arguments:
 *  - Version (SECRO or CjRDT)
 *  - [Commit interval (number >= 0)]  (only if version is SECRO)
 *  - Max number of operations (e.g. 1000 indicates to execute the benchmark for: 1, 2, 3, ..., 1000 operations in the history)
 *  - File to write execution times to
 */

// Load the correct module, depending on the version
const version = (process.argv[2]).toUpperCase();
var TangentService, commitInterval, maxOps, file, gcFile;
switch (version) {
    case 'SECRO':
        TangentService = require('../TangentService');
        commitInterval = parseInt(process.argv[3]);
        maxOps         = parseInt(process.argv[4]);
        file           = process.argv[5];
        break;
    case 'CJRDT':
        TangentService = require('../TangentCjRDT');
        maxOps         = parseInt(process.argv[3]);
        commitInterval = maxOps + 10; // such that we never attempt to commit
        file           = process.argv[4];
        break;
    default:
        throw new Error('Unrecognized version.');
}

// Register function to benchmark
benchmarker.setFn(() => tanService.tan.compute());

// Register setup function
benchmarker.setSetup(() => {
    tanService = new TangentService(); // global variable
    ctr = 0; // global variable
});

// Register function to execute after every execution of `compute`
benchmarker.setAfterFn(() => {
    // Commit at the specified interval
    if (ctr !== 0 && ctr % commitInterval === 0)
        tanService.tan.commit();
    ctr++;
});

// ----- Run the benchmark -----
const xRange = Array.from(new Array(maxOps), (x,i) => i+1);
benchmarker.run(xRange, file);