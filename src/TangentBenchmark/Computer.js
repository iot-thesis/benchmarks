// Computes 10K tangents and returns the result of the last computation.

%NeverOptimizeFunction(computeTans); // avoid compiler optimizations

function computeTans() {
    var res;
    for (var i = 0; i < 10000; i++) {
        const degrees = i;
        res = Math.tan(degrees * Math.PI/180);
    }
    return res;
}

module.exports = computeTans;