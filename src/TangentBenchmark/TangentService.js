const cs = require('cscript');
const Tangent = require('./Tangent');

/*
 * Artificial example where operations of a SECRO are in constant time.
 * The SECRO's state also remains constant (i.e. does not become smaller/bigger).
 * This example allows to see the performance of SECROs.
 */

class TangentService extends cs.Service {
    constructor() {
        super();
        this.tan = super.makeReplica('tan', new Tangent());
    }
}

cs.Factory.registerCmRDTClass(Tangent);

module.exports = TangentService;