const cjrdt = require('cjrdt').default;
const computeTans = require('./Computer');

class Tangent {
    constructor() {
        cjrdt.reset();
        cjrdt.doc = { res: 0 };
        this.tan = { compute: () => cjrdt.doc.res = computeTans() }
    }
}

module.exports = Tangent;