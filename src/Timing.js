const writeToFile = require('./FileWriter');
const { performance } = require('perf_hooks');

/*
 * Disable compiler (JIT) optimizations for these functions.
 * Requires running node with the --allow-natives-syntax flag.
 */
%NeverOptimizeFunction(bench);
%NeverOptimizeFunction(time);
%NeverOptimizeFunction(run);

var fn = () => {}, setup = () => {}, afterFn = () => {};

// ---------- SETUP FUNCTIONS ----------

// Sets the function to benchmark
function setFn(timedFn) {
    fn = timedFn;
    //%NeverOptimizeFunction(fn);
}

// Registers a function that is called after every execution of `fn`.
function setAfterFn(fn) {
    afterFn = fn;
}

// Registers a function to call before every benchmark
function setSetup(fun) {
    setup = fun;
}


// ---------- BENCHMARKING FUNCTIONS ----------
function run(xs, file) {
    /*
     * Perform some warmup rounds.
     * Notice that these warmup rounds only works if we write them manually,
     * i suspect that a for loop gets optimized in some way.
     */
    console.log("calling setup");
    setup();
    console.log("WARMUP 1");
    bench(xs);
    setup();
    console.log("WARMUP 2");
    bench(xs);
    setup();
    console.log("WARMUP 3");
    bench(xs);
    setup();
    console.log("WARMUP 4");
    bench(xs);
    setup();
    console.log("WARMUP 5");
    bench(xs);
    setup();

    // Run the actual benchmark after a delay of 50ms
    // In order to allow the GC to kick in before we start.
    setTimeout(() => {
        const samples = bench(xs);

        // Write time measurements to file
        var data = "x\tDuration\tRSS\tHeapTotal\tHeapUsed\tExternal\n";
        samples.forEach(sample => {
            const [x, duration, {rss, heapTotal, heapUsed, external}] = sample;
            data += `${x}\t${duration}\t${rss}\t${heapTotal}\t${heapUsed}\t${external}\n`;
        });

        //const file = process.argv[2];
        console.log("Going to write to " + file);
        writeToFile(data, file, process.exit);
    }, 50);
}

/*
 * Benchmarks the execution time of a given function for various values of x.
 * i.e. measures the dependent variable y in function of the the independent variable x.
 * Returns an array of samples, where a sample is an array containing x and y: [x, y] (y = measured execution time)
 */
function bench(xs) {
    const samples = [];

    // Store initial memory usage
    const initialSample = [xs[0]-1, 'NA', process.memoryUsage()];

    // Proceed with the actual benchmark
    // Synchronous execution
    for (var x of xs) {
        const duration = time(fn.bind(null, x));
        const memUsage = process.memoryUsage();
        samples.push([x, duration, memUsage]);
        afterFn();
    }

    return [initialSample, ...samples];
}

// Returns the time to execute function `fn` as well as the result
function time(fn) {
    performance.mark(`start`); // Start time //

    const res = fn();

    performance.mark(`end`);
    performance.measure(`duration`, `start`, `end`); // time it took to compute 10K tangents

    const perfEntries = performance.getEntriesByName(`duration`); // array of PerformanceEntry objects
    var duration = perfEntries[0].duration;

    // Correct duration if garbage collection was triggered
    const gcEntries = performance
        .getEntriesByType('gc')
        // Only retain entries that happened between both markers
        .filter(entry => entry.start >= perfEntries[0].start && entry.start <= perfEntries[0].end);

    // Remove GC durations from `duration`
    gcEntries.forEach(entry => {
        // Only retain entries that happened between both markers
        var gcEnd = gcStart + entry.duration,
            gcDuration = entry.duration;

        if (gcEnd > perfEntries[0].end) {
            console.log("Unexpected");
            gcDuration -= gcEnd - perfEntries[0].end;
        }

        GCs++;
        console.log(`GC duration: ${gcDuration} ms`);
        duration -= gcDuration;
    });

    // Clear the performance timeline
    performance.clearMarks('start');
    performance.clearMarks('end');
    performance.clearMeasures();
    performance.clearGC();

    return duration;
}

module.exports.setFn = setFn;
module.exports.run = run;
module.exports.setSetup = setSetup;
module.exports.setAfterFn = setAfterFn;