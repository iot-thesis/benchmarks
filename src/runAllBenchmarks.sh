#!/usr/bin/env bash

#
# Runs all benchmarks for the artificial tangent example
# as well as for the collaborative text editor example.
#
# For the tangent example:
#  1) Execution time in function of the operation history's size, and for different commit intervals
#  2) Time to consistency in function of the number of operations
#
# For the collaborative text editor:
#  1) Execution time in function of te operation history's size, and for different commit intervals
#  2) Memory usage in function of operation history's size, for different commit intervals
#  3) Time to consistency in function of the number of operations
#
# NOTE: All benchmarks are meant to run on the `isabelle` cluster.
#       The benchmarks might not run on your local machine as it relies on unix commands (e.g. `timeout`).
#
# IMPORTANT:
#       The benchmarks rely on an `Examples` folder located at the same level as the `Benchmarks` folder.
#       The `Examples` folder should contain the three versions of the text editor:
#         `TextEditor`       : SECRO version
#         `TreeTextEditor`   : Tree-based SECRO version
#         `CjRDT-TextEditor` : CjRDT version
#       These 3 versions are treated as local dependencies (see package.json file).
#       Make sure to compile each version to regular javascript!
#

##########################
# Run tangent benchmarks #
##########################

################################
# Run execution time benchmark #
################################

echo "$(date +%H:%M:%S): Starting PART 1: tangent benchmarks..."
echo -e "\t$(date +%H:%M:%S): Starting execution time benchmarks..."

# Without commits
echo -e "\t\t$(date +%H:%M:%S): SECRO without commits"
ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TangentBenchmark/ExecTime/TimeBenchmark.js 60 /out/benchmarks/Tangent/time/SECRO/ SECRO 1000 500 > /dev/null 2>&1

# For different commit intervals
for interval in 1 10 20 50 100
do
    echo -e "\t\t$(date +%H:%M:%S): commit interval: $interval"
    ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TangentBenchmark/ExecTime/TimeBenchmark.js 60 /out/benchmarks/Tangent/time/SECRO_C"$interval"/ SECRO $interval 1000 > /dev/null 2>&1
done

# For CjRDT
echo -e "\t\t$(date +%H:%M:%S): CjRDT version"
ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TangentBenchmark/ExecTime/TimeBenchmark.js 60 /out/benchmarks/Tangent/time/CjRDT/ CjRDT 1000 > /dev/null 2>&1

#####################################
# Run time to consistency benchmark #
#####################################

echo -e "\t$(date +%H:%M:%S): Starting TTC benchmarks..."

# For different commit intervals
for interval2 in 10 20 50 100
do
    echo -e "\t\t$(date +%H:%M:%S): commit interval: $interval2..."
    ./RunTTCBenchmark.sh ./TangentBenchmark/TimeToConsistency/TimeToConsistencyBenchmark.js SECRO $interval3 /out/benchmarks/Tangent/TTC/ttc.txt 10 50 100 150 250 300 400 500
done

# For CjRDT
echo -e "\t\t$(date +%H:%M:%S): CjRDT version"
./RunTTCBenchmark.sh ./TangentBenchmark/TimeToConsistency/TimeToConsistencyBenchmark.js CjRDT 0 /out/benchmarks/Tangent/TTC/ttc.txt 10 50 100 150 250 300 400 500


############################################
# Run collaborative text editor benchmarks #
# For all three versions: naive SECRO,     #
# tree-based SECRO and CjRDT version.      #
############################################

################################
# Run execution time benchmark #
################################

echo "$(date +%H:%M:%S): Starting PART 2: collaborative text editor benchmarks..."
echo -e "\t$(date +%H:%M:%S): Starting execution time benchmarks..."

versions='SECRO TREE'
for version in $versions
do
    echo "\t\t$version version"

    # Without commits
    echo -e "\t\t\t$(date +%H:%M:%S): without commits"
    ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TextEditorBenchmarks/ExecTime/TimeBenchmark.js 60 /out/benchmarks/TextEditor/time/"$version"/ "$version" 1000 500 > /dev/null 2>&1

    # For different commit intervals
    for interval3 in 1 10 20 50 100
    do
        echo -e "\t\t\t$(date +%H:%M:%S): commit interval: $interval"
        ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TextEditorBenchmarks/ExecTime/TimeBenchmark.js 60 /out/benchmarks/TextEditor/time/"$version"_C"$interval"/ "$version" $interval3 1000 > /dev/null 2>&1
    done
done

# For CjRDT
echo -e "\t\t$(date +%H:%M:%S): CjRDT version"
ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; ./runTimeBenchmark.sh ./TextEditorBenchmarks/ExecTime/TimeBenchmark.js 60 /out/benchmarks/TextEditor/time/CjRDT/ CjRDT 1000 > /dev/null 2>&1

##############################
# Run memory usage benchmark #
##############################

echo -e "\t$(date +%H:%M:%S): Starting memory usage benchmarks..."

for version2 in $versions
do
    echo "\t\t$version2 version"

    # Without commits
    echo -e "\t\t\t$(date +%H:%M:%S): without commits"
    ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; node --allow-natives-syntax --expose-gc ./TextEditorBenchmarks/Memory/MemoryBenchmark.js "$version2" 510 500 /out/benchmarks/TextEditor/memory/"$version2"/run.txt > /dev/null 2>&1

    # For different commit intervals
    for interval4 in 1 10 20 50 100
    do
        echo -e "\t\t\t$(date +%H:%M:%S): commit interval: $interval4"
        ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; node --allow-natives-syntax --expose-gc ./TextEditorBenchmarks/Memory/MemoryBenchmark.js "$version2" $interval4 1000 /out/benchmarks/TextEditor/memory/"$version2"_C"$interval4"/run.txt > /dev/null 2>&1
    done
done

# For CjRDT
echo -e "\t\t$(date +%H:%M:%S): CjRDT version"
ssh kdeporre@isabelle-a -t -T cd Benchmarks/src; node --allow-natives-syntax --expose-gc ./TextEditorBenchmarks/Memory/MemoryBenchmark.js CjRDT 1000 /out/benchmarks/TextEditor/memory/CjRDT/run.txt > /dev/null 2>&1

#####################################
# Run time to consistency benchmark #
#####################################

echo -e "\t$(date +%H:%M:%S): Starting TTC benchmarks..."

for version3 in $versions
do
    echo "\t\t$version3 version"

    # For different commit intervals
    for interval5 in 10 20 50 100
    do
        echo -e "\t\t$(date +%H:%M:%S): commit interval: $interval5..."
        ./RunTTCBenchmark.sh ./TextEditorBenchmarks/TimeToConsistency/TimeToConsistencyBenchmark.js "$version3" $interval5 /out/benchmarks/TextEditor/TTC/ttc.txt 10 50 100 150 250 300 400 500
    done
done

# For CjRDT
echo -e "\t\t$(date +%H:%M:%S): CjRDT version"
./RunTTCBenchmark.sh ./TextEditorBenchmarks/TimeToConsistency/TimeToConsistencyBenchmark.js CjRDT 0 /out/benchmarks/TextEditor/TTC/ttc.txt 10 50 100 150 250 300 400 500