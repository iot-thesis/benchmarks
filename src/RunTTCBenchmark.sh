#!/usr/bin/env bash

#
# This script runs the Time To Consistency benchmark on all nodes of isabelle concurrently.
# The benchmark is executed for both versions (state validators and CjRDT).
# Each version is tested for different amounts of operations (passed as arguments to this script).
# Every test is repeated 30 times to get a normal distribution of the measurements.
#
# Example call:
# ./RunTTCBenchmark.sh 1 /out/ttc.txt 10 20 50 100
#
# The benchmark is performed using 10 nodes and isabelle-a (10.0.0.10) is the coordinator for the benchmark.
# 1 is the commit interval and /out/ttc.txt is the file in which the results will be written,
# the remaining arguments are the total number of operations to benchmark.
#
# Technicalities about the bash script were found at:
# https://unix.stackexchange.com/questions/242087/run-commands-in-parallel-and-wait-for-one-group-of-commands-to-finish-before-sta
# https://stackoverflow.com/questions/10028820/bash-wait-with-timeout
# https://stackoverflow.com/questions/1570262/shell-get-exit-code-of-background-process
#
# If something goes wrong and the processes are still running in the background.
# Then execute:
# for node in {a..j}
# do
#     ssh kdeporre@isabelle-"$node" -t pkill -u kdeporre -f 'node'
# done
#

repetitions=30
#versions='SV CJRDT'

master="10.0.0.10" # ip of the master node (isabelle-a)
nodes=10 #$1  # number of nodes
benchFile=$1
version=$2
commitInterval=$3
output=$4 # result file

# "eat" the first two arguments
shift
shift
shift
shift

# Runs the benchmark on all worker nodes (a to j)
bench() {
    pids=() # time out PIDs
    for n in {a..j}
    do
        # & to run in background
        # > /dev/null to redirect the script's output such that it does not pollute the command line
        timeout -k 240s 240s ssh kdeporre@isabelle-"$n" -t -T cd Benchmarks/src; node --allow-natives-syntax $benchFile "$@" > /dev/null 2>&1 &
        pids+=($!) # store PID of the last background process that was executed
    done

    # Set up a barrier that waits for the master background process to finish.
    # Due to a SpidersJS bug, a process might never return.
    # Therefore, we applied a time out of 2 minutes, after which we'll retry the operation.
    if wait "${pids[0]}"; then
        ./killAll.sh > /dev/null 2>&1 # because some processes might have missed the master's end message and are still running in idle mode...
    else
        echo -e "\t\t$(date +%H:%M:%S): Run timed out!"
        echo -e "\t\tKilling processes"
        ./killAll.sh > /dev/null 2>&1
        echo -e "\t\tRetrying..."
        bench "$@"
    fi
}

#for version in $versions
#do
#echo "Benchmarking $version"
for totalOps in "$@"
do
    echo -e "\t\t\t$totalOps operations"
    for ((i=1; i<=$repetitions; i++))
    do
        echo -e "\t\t\t\t$(date +%H:%M:%S): run $i"
        if [ "$version" == "CjRDT" ]; then
            bench $version $master $totalOps $nodes $output
        else
            bench $version $master $commitInterval $totalOps $nodes $output
        fi
    done
done
#done