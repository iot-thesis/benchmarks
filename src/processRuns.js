const fs = require('fs'),
      path = require('path'),
      Immutable = require('immutable'),
      writeToFile = require('./FileWriter');

/*
 * Processes all runs of a benchmark,
 * in order to create a summary containing:
 *  - value of the independent variable for which the measurement was made (i.e. x value)
 *  - median execution time for the given value of x
 *  - 95% CI for each value of x
 *  - Memory usage statistics for each value of x
 *
 *  Note: All measurements where garbage collection may have occured are dropped.
 *        Garbage collections are found when the heap usage decreases from one run to another.
 *        The garbage collection might have happened between both runs or within the second run.
 *        Since there is a delay on the GC marker in the timeline, we cannot distinguish between them.
 *        Therefore, we always drop measurements when there is proof that GC happened.
 */

const sampleDir  = process.argv[2],
      summaryDir = process.argv[3];

//const FILTERED = "FILTERED";
//const EMPTY_ROW = new Array(6).fill(FILTERED);
const X_IDX = 0, Y_IDX = 1;
var rows = Immutable.List();

// List all sample files
fs.readdir(path.join(__dirname, sampleDir), (err, files) => {
    if (err) throw err;
    const samples = [];
    var xValues = [];

    // Read all runs
    files.forEach(file => {
        if (file.startsWith("run")) {
            // Read the file containing the execution times
            const data = fs.readFileSync(path.join(__dirname, sampleDir, file), "utf8");
            var info = Immutable.List(data.split('\n').map(str => {
                const [x, y, ...memStats] = str.split('\t');
                return [parseInt(x), parseFloat(y), ...memStats.map(parseFloat)];
            })).shift().pop(); // remove headers and last entry resulting from the trailing newline at the end of the file

            // Remove measurements that could have been affected by GC.
            // i.e. remove the rows where heap usage drops.
            info = info.filter((row, idx) => {
                const HEAP_USED_IDX = 4;
                if (idx === 0)
                    return true;
                else
                    return row[HEAP_USED_IDX] >= info.get(idx - 1)[HEAP_USED_IDX];
            });

            rows = rows.push(...info.map(Immutable.List)); // append the retained rows

            /*
            const xs = info.map(i => i[0]).toJS(), // x values
                  measurements = info.map(i => i.slice(1));

            xValues = xs;
            samples.push(measurements);
            */
        }
    });

    // Compute the median of all rows
    const //s = Immutable.fromJS(samples),
          grouped = rows.groupBy(row => row.get(X_IDX)) // Group all rows that measured the same `x`
                        .map(samples => samples.map(row => row.get(Y_IDX))); // retain only execution time
          //zipped = s.get(0).zip(...s.rest()) // zips all corresponding measurements (i.e. for each `x` zips its measurements together)
          //          .map(measurements => measurements.filter(x => x !== FILTERED));

    var sampleSizes = Immutable.Map();
    const groupedMedians = grouped.map(median),
          confIntervals = grouped.map((samples, x) => {
              const N = samples.size;
              sampleSizes = sampleSizes.set(x, N);
              return confInterval(samples);
          });

    // Write the medians and their 95% CI to a summary file
    var data = "x\tMedianTime\tlowerCI\tupperCI\tSampleSize\n";
    grouped.forEach((y, x) => {
        const median     = groupedMedians.get(x),
              ci         = confIntervals.get(x),
              lowerCI    = ci.get(0),
              upperCI    = ci.get(1),
              sampleSize = sampleSizes.get(x);
        data += `${x}\t${median}\t${lowerCI}\t${upperCI}\t${sampleSize}\n`;
    });

    console.log("written to: " + path.join(__dirname, summaryDir, "summary.txt").toString());
    writeToFile(data, summaryDir, process.exit);

    /*
    zipped.forEach((rows, idx) => {
       console.log(`\nAt idx ${idx} found rows:`);
       rows.forEach(row => console.log(row));
    });

    const times = zipped.shift().toJS(); // execution times + memory usage stats (shift removes the first row which contains only the initial memory usage stats)
    var medians = times.map(samples => median(samples)); //mean(samples));
    */

    /*
    // Compute the 95% confidence intervals for all `x`
    const confIntervals = grouped.map

    // Compute the 95% confidence intervals for all rows
    const confidenceIntervals = [], sampleSizes = [];
    for (var i = 0; i < times.length; i++) {
        const samples = times[i].map(s => s[0]), // map to retain only the execution times
              //mean = medians[i], // sample mean
              N = times[i].length;
        sampleSizes.push(N);
        var ttt = false;
        if (i === 778) ttt = true;
        confidenceIntervals.push(confInterval(samples, ttt));
    } */

    /*
    // Compute the standard deviation of all rows
    const stdevs = [], sampleSizes = [];
    for (var i = 0; i < times.length; i++) {
        const samples = times[i],
              mean = means[i], // sample mean
              N = times[i].length;
        sampleSizes.push(N);
        stdevs.push(stdev(samples, mean, N));
    }*/

    /*
    // Write the means and standard deviations to a summary file
    var data = "x\tMeanTime\tStdev\tSampleSize\n";
    means.forEach((meanTime, idx) => {
        data += `${xValues[idx]}\t${meanTime}\t${stdevs[idx]}\t${sampleSizes[idx]}\n`;
    });
    */

    /*
    // Write the medians and their 95% CI to a summary file
    var data = "x\tMedianTime\tlowerCI\tupperCI\tSampleSize\tRSS\tHeapTotal\tHeapUsed\tExternal\n";
    //medians = [ zipped.get(0)[0], ...medians]; // prepend the row containing the initial memory statistics
    // Add row with initial memory usage stats
    data += `${xValues[0]}\tNA\tNA\tNA\tNA\t${samples[0].get(0)[1]}\t${samples[0].get(0)[2]}\t${samples[0].get(0)[3]}\t${samples[0].get(0)[4]}\n`;

    medians.forEach((median, idx) => {
        const [medianTime, rss, heapTotal, heapUsed, external] = median;
        data += `${xValues[idx+1]}\t${medianTime}\t${confidenceIntervals[idx][0]}\t${confidenceIntervals[idx][1]}\t${sampleSizes[idx]}\t${rss}\t${heapTotal}\t${heapUsed}\t${external}\n`;
    });

    console.log("written to: " + path.join(__dirname, summaryDir, "summary.txt").toString());
    writeToFile(data, summaryDir, process.exit);
    */
});

function mean(a) {
    var sum = 0;
    for (var i = 0; i < a.length; i++)
        sum += a[i];
    return sum / a.length;
}

// Computes the standard deviation of a sample
function stdev(samples, mean, N) {
    var devSum = 0; // sum of the deviations
    for (var i = 0; i < samples.length; i++) {
        const x = samples[i];
        devSum += Math.pow(x - mean, 2); // |x - u|^2 (squared distance to mean)
    }
    var meanDev = devSum / (N - 1); // mean deviation
    return Math.sqrt(meanDev);
}

///////////////////
// Takes an array of arrays: [ [time, RSS, heapTotal, heapUsed, external], ...]
// Computes the median based on the time entries.
// Returns the median entries (median of the times as well as the corresponding memory statistics)
function medianOLD(a) {
    const sorted = a.sort((a, b) => a[0] - b[0]); // sort on time, ascending order
    if (sorted.length % 2 === 1) {
        // Odd length, pick the middle one
        const middle = Math.ceil(sorted.length / 2); // actual index of the middle element is one lesser
        return sorted[middle-1];
    }
    else {
        // Even length, take the mean of the two middle values
        const middle = sorted.length / 2; // index of the 2nd middle element
        const e1 = Immutable.fromJS(sorted[middle-1]);
        const e2 = Immutable.fromJS(sorted[middle]);
        return e1.zip(e2).map(sum).map(e => e / 2).toJS(); // median of all corresponding values
        //return Immutable.fromJS(sorted[middle-1] + sorted[middle]) / 2;
    }
}

function median(a) {
    const sorted = a.sort((a, b) => a - b); // sort on time, ascending order
    if (sorted.size % 2 === 1) {
        // Odd length, pick the middle one
        const middle = Math.ceil(sorted.size / 2); // actual index of the middle element is one lesser
        return sorted.get(middle-1);
    }
    else {
        // Even length, take the mean of the two middle values
        const middle = sorted.size / 2; // index of the 2nd middle element
        /*
        const e1 = sorted.get(middle-1);
        const e2 = sorted.get(middle);
        return e1.zip(e2).map(sum).map(e => e / 2);//.toJS(); // median of all corresponding values
        */
        return (sorted.get(middle-1) + sorted.get(middle)) / 2;
    }
}

// Returns the 95% CI for the median of a sample
function confIntervalOLD(samples, ttt) {
    const sorted = samples.sort((a, b) => a - b), // sort on time, ascending order
        n = samples.length,
        llElem = Math.ceil((n / 2) - ((1.96 * Math.sqrt(n))/2)),     // llElem-th element is the lower limit of the 95% CI
        ulElem = Math.ceil(1 + (n / 2) + ((1.96 * Math.sqrt(n))/2)), // ulElem-th element is the upper limit of the 95% CI
        lowerLimit = sorted[llElem-1],
        upperLimit = sorted[ulElem-1];

    /*
    if (ttt) {
        console.log(`Samples for 779: [${llElem}, ${ulElem}]`);
        console.log(samples);
    }*/

    /*
    console.log("95% CI:");
    console.log("n is " + n);
    console.log(sorted);
    console.log("Lower limit index: " + (llElem-1) + " Upper limit index: " + (ulElem-1));
    */
    //console.log(`95% CI: [${lowerLimit} ; ${upperLimit}]`);

    return [lowerLimit, upperLimit];
}

// Returns the 95% CI for the median of a sample
function confInterval(samples, ttt) {
    const sorted = samples.sort((a, b) => a - b), // sort on time, ascending order
        n = samples.size, // 1.5 - ((1.96*sqrt(2))/2) --> 0   --> 1
        llElem = Math.abs(Math.ceil((n / 2) - ((1.96 * Math.sqrt(n))/2))),     // llElem-th element is the lower limit of the 95% CI
        ulElem = Math.abs(Math.ceil(1 + (n / 2) + ((1.96 * Math.sqrt(n))/2))), // ulElem-th element is the upper limit of the 95% CI
        lowerLimit = (llElem - 1) < 0 ? sorted.get(0) : sorted.get(llElem-1),
        upperLimit = sorted.get(ulElem-1, sorted.get(n-1));

    return Immutable.List([lowerLimit, upperLimit]);
}

function sum(list) {
    return list.reduce((sum, x) => sum + x, 0);
}