const fs = require('fs'),
      path = require('path');

// Taken from: https://stackoverflow.com/questions/13542667/create-directory-when-writing-to-file-in-node-js
function ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
}

// Writes the data to the specified file.
// If the file did not yet exist, it first writes the header to the file.
// When the data has been written, `callback` is invoked.
function writeToFile(data, file, callback = () => {}, header = "") {
    const filePath = path.join(__dirname, file);
    console.log("file path: " + filePath.toString());
    const newFile = !fs.existsSync(filePath);
    if (newFile)
        data = header + data;
    ensureDirectoryExistence(filePath);
    fs.writeFile(filePath.toString(), data, { flag: "a" }, function(err) {
        if(err)
            console.log(`Could not write to file: ${err}`);
        callback();
    });
}

module.exports = writeToFile;