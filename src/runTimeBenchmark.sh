#!/usr/bin/env bash

# Example usage:
#   ./runTimeBenchmark.sh OpHistoryBenchmark.js 30 /out/benchmarks/test/ SV 10 1000
# Runs the benchmark 30 times, for the state validators version, with a commit interval of 10 for 1000 operations.

file="$1"      # File to execute a couple of times
repetitions=$2 # Number of times to repeat the test (should be at least 30!)
directory="$3" # Output directory

# "eat" the first three arguments
shift
shift
shift

echo "Starting benchmark..."

for ((i=1; i<=$repetitions; i++))
do
    echo "Starting run $i"
    # "$@" expands the remaining arguments
    node --allow-natives-syntax $file "$@" "$directory/runs/run$i.txt"
    echo "Ended run $i"
done

echo "End of benchmark"
echo "Processing runs..."

node processRuns "$directory/runs/" "$directory/summary.txt"

echo "Runs processed, summary has been written"