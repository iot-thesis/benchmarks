# BEFORE PLOTTING ALWAYS FIRST RESTART R SESSION !!! :
#rm(list = ls())
#.rs.restartR()

library(ggplot2)
library(dplyr)
requireNamespace("plyr")

# Plots the execution time with 95% confidence intervals.
# Execution time is measured in function of the number of executed operations (i.e. operation history size).

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

# Merges the files of all runs into one dataset
# Based on: https://www.r-bloggers.com/merge-all-files-in-a-directory-using-r-into-a-single-dataframe/
mergeRuns <- function(dir, version) {
  file_list <- list.files(dir)
  for (file in file_list) {
    path <- paste(dir, file, sep="/")
    print("path")
    print(path)
    # if the merged dataset doesn't exist, create it
    if (!exists("dataset")) {
      dataset <- na.omit(read.table(path, header=TRUE, sep="\t")) %>%
        plyr::rename(replace = c("TotalDuration" = "Duration"), warn_missing = FALSE) %>%
        filter(Duration != "Duration") %>%
        mutate(x = as.numeric(as.character(x)),
               HeapUsed = as.numeric(as.character(HeapUsed)),
               Duration = as.numeric(as.character(Duration))) %>%
        filter(x != 0) %>%                   # Removes rows where x == 0
        filter(HeapUsed > lag(HeapUsed)) %>% # Removes rows affected by GC
        as.data.frame()
      print("a")
      print(paste("Dataset length: ", nrow(dataset)))
    }
    
    # if the merged dataset does exist, append to it
    else {
      temp_dataset <- na.omit(read.table(path, header=TRUE, sep="\t")) %>%
        plyr::rename(replace = c("TotalDuration" = "Duration"), warn_missing = FALSE) %>%
        filter(Duration != "Duration") %>%
        mutate(x = as.numeric(as.character(x)),
               HeapUsed = as.numeric(as.character(HeapUsed)),
               Duration = as.numeric(as.character(Duration))) %>%
        filter(x != 0) %>%                   # Removes rows where x == 0
        filter(HeapUsed > lag(HeapUsed)) %>% # Removes rows affected by GC
        as.data.frame()
      print(paste("file: ", path))
      print(paste("TempDataset length: ", nrow(temp_dataset)))
      print(paste("names", names(dataset), names(temp_dataset), sep="/"))
      names(dataset) <- names(temp_dataset)
      dataset<-rbind(dataset, temp_dataset)
      rm(temp_dataset)
    }
  }
  
  # Add a "Version" column which indicates the version
  dataset$Version <- rep(c(version), times=nrow(dataset))
  return(dataset)
}

processRuns <- function(data) {
  d <- data %>%
    group_by(x) %>%
    mutate(n = n(),
           MeanDuration = mean(Duration),
           Version = Version,
           stdev = sd(Duration),
           lowerCI = MeanDuration - (qnorm(0.975) * stdev / sqrt(n)),
           upperCI = MeanDuration + (qnorm(0.975) * stdev / sqrt(n))) %>%
    distinct(x, .keep_all=TRUE) %>%
    filter(n >= 30) %>% # Filter our rows with less than 30 samples, because we cannot apply the central limit theorem
    #filter(x %% 10 == 0) %>% # Retain only 1 sample every 10 samples, to get a smooth curve
    select(x, MeanDuration, n, stdev, lowerCI, upperCI, Version) %>%
    arrange(x) %>%
    as.data.frame()
  
  return(d)
}

# this works well: secroRuns %>% filter(x==2 | x==3) %>% filter(HeapUsed > lag(HeapUsed)) %>% filter(x==3)
# but the above doesn't... (n = 32, why??? should be like 90)

# Load and process each version
versionPath <- "../../../data/TextEditor/time-100insertsPerOperation/"
#secro1Runs <- mergeRuns(paste(versionPath, "SECRO_C1", sep = ""), "List C1")
#secro1Data <- processRuns(secro1Runs)

#secro100Runs <- mergeRuns(paste(versionPath, "SECRO_C40", sep = ""), "List C100")
#secro100Data <- processRuns(secro100Runs)

#tree1Runs <- mergeRuns(paste(versionPath, "TREE_C1", sep = ""), "Tree C1")
#tree1Data <- processRuns(tree1Runs)

secroRuns <- mergeRuns(paste(versionPath, "SECRO", sep = ""), "List")
secroData <- processRuns(secroRuns)

treeRuns <- mergeRuns(paste(versionPath, "TREE", sep = ""), "Tree")
treeData <- processRuns(treeRuns)

cjrdtRuns <- mergeRuns("../../../data/TextEditor/time-100insertsPerOperation/CjRDT", "CjRDT")
cjrdtData <- processRuns(cjrdtRuns) %>% filter(x <= 50) %>% as.data.frame()

# Combine the dataframes of all versions
data <- do.call("rbind", list(secroData, treeData, cjrdtData))
#data <- do.call("rbind", list(secroData, treeData, secro1Data, tree1Data, secro100Data))

#data <- data %>% mutate(Type = ifelse(Version == "CjRDT", "CRDT", "SECRO"))

# Plot different versions in a different colour
# and different commit intervals in a different line type.
ggplot(data, aes(x = x, y = MeanDuration, color=Version)) + #, linetype = Type)) + # linetype=Version
  geom_line() +
  #scale_linetype_manual(values=c("dashed", "solid"), 
  #                      breaks=c("SECRO", "CRDT")) +
  #geom_ribbon(aes(ymin = data$lowerCI, ymax = data$upperCI), alpha=0.2, linetype="solid") +
  geom_errorbar(aes(ymin = data$lowerCI, ymax = data$upperCI), alpha=0.8) +
  # Transform the labels of the x-axis (50 operations becomes 5000 characters because each op inserts 100 characters)
  scale_x_continuous(labels = function(x)x*100) +
  labs(#title="Execution Time", 
       #subtitle="For the different versions, in function of the number of operations.",
       #caption = "Bands represent the 95% CI.\nSECRO versions committed after every operation.",
       x="Document Length",
       y="Time in milliseconds") +
  theme_bw() +
  # Formatting of the legend
  #theme(legend.position = "top") + # put legend above the graph
  #theme(legend.title=element_blank()) + # removes the legend title
  scale_color_discrete(name="Version",
                       breaks=c("List", "Tree", "List C100", "Tree C1", "List C1", "CjRDT"), 
                       labels=c(" List SECRO    ", " Tree SECRO    ", "List C100", "Tree C1", "List C1", " Json CRDT")) +
  guides(color=guide_legend(override.aes=list(fill=NA))) +
  #theme(legend.background = element_rect(size=.5, linetype="solid", colour="black")) # Add boxes around legends
  theme(legend.position = "top",
        legend.text=element_text(size=15),
        legend.title=element_blank(), #element_text(size=17),
        axis.text=element_text(size=15),
        axis.text.y=element_text(size=15),
        axis.text.x=element_text(size=15),
        axis.title=element_text(size=15,face="bold"))
#theme(legend.background = theme_rect(col = 0))

