library(dplyr)

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

###### LOAD DATA ########
runData   <- read.table("../../../data/TextEditor/memory/TREE_C1/run.txt",  header=TRUE)
secroData <- read.table("../../../data/TextEditor/memory/SECRO_C1/run.txt", header=TRUE)
cjrdtData <- read.table("../../../data/TextEditor/memory/CjRDT/run.txt",    header=TRUE)
#runData$x <- runData$x + 1 # Because "x" started from 0, but we want operations to be numbered starting from 1
#colnames(runData)[ncol(runData)] <- "Operation"

# Group measurements by "x" value.
# Then, for each "x" value compute the mean,
# standard deviation and 95% confidence intervals.

processData <- function(data) {
  data$x <- data$x + 1 # Because "x" started from 0, but we want operations to be numbered starting from 1
  colnames(data)[ncol(data)] <- "Operation"
  
  d <- data %>%
    group_by(x) %>%
    select(x, HeapUsed, Operation)  %>%
    mutate(n = n(),
           Operation = Operation,
           MHeapUsed = mean(HeapUsed), 
           stdev = sd(HeapUsed),
           lowerCI = MHeapUsed - (qnorm(0.975) * stdev / sqrt(n)),
           upperCI = MHeapUsed + (qnorm(0.975) * stdev / sqrt(n))) %>%
    distinct(x, .keep_all=TRUE) %>%
    #filter(x != 0) %>%
    arrange(x) %>%
    as.data.frame()
  
  return(d)
}

#processedData <- runData %>%
#                    group_by(x) %>%
#                    select(x, HeapUsed, Operation)  %>%
#                    mutate(n = n(),
#                           Operation = Operation,
#                           MHeapUsed = mean(HeapUsed), 
#                           stdev = sd(HeapUsed),
#                           lowerCI = HeapUsed - (qnorm(0.975) * stdev / sqrt(n)),
#                           upperCI = HeapUsed + (qnorm(0.975) * stdev / sqrt(n))) %>%
#                    distinct(x, .keep_all=TRUE) %>%
#                    #filter(x != 0) %>%
#                    arrange(x) %>%
#                    as.data.frame()

processedData <- processData(runData)
secroData <- processData(secroData)
cjrdtData <- processData(cjrdtData)

idxs <- c(2, seq(from = 11, to = 1001, by = 10))

df <- processedData[idxs,]
secroData <- secroData[idxs,]
cjrdtData <- cjrdtData[idxs,]

secroData$Version <- rep(c("List"), times=nrow(secroData))
cjrdtData$Version <- rep(c("CjRDT"), times=nrow(cjrdtData))

#df <- processedData
########################

# Add version to each row
df$Version <- rep(c("Tree"), times=nrow(df))

# Compute the intervals of the consecutive operations
# Based on: https://stackoverflow.com/questions/32529854/group-data-in-r-for-consecutive-rows
opData <- processedData[-1,] # remove the first row which is the initial memory usage
opData$Op <- with(opData, ifelse(Operation == "insert", 1, 2))

operationIntervals <- opData %>%
  group_by(Operation, group_weight = cumsum(c(1, diff(Op) != 0))) %>%
  summarise(xStart = min(x), xEnd = max(x)) %>%
  arrange(xStart) # sort the groups in ascending order

library(ggplot2)

yrng <- range(df$HeapUsed / 1024 / 1024)

# Dataframe for all three version
#df <- rbind(df, secroData, cjrdtData)

# Dataframe for list and tree SECROs only
df <- rbind(df, secroData)



# Plot heap space usage
ggplot(df, aes(x = x, y = MHeapUsed / 1024 / 1024, color=Version)) + #(df, aes(x = x, y = HeapUsed / 1024 / 1024)) + #, color=Version) +
  scale_color_manual(values = c("Tree" = "blue", "List" = "firebrick", "CjRDT" = "black"),
                     breaks=c("CjRDT", "Tree", "List"), 
                     labels=c(" Json CRDT  ", " Tree  ",  " List")) +
  geom_line(aes(lty="data")) + #, color="Blue") +
  geom_smooth(method='lm', se=TRUE, aes(lty="trend"), show.legend = FALSE) + #linetype="dashed", show_guide = FALSE) +
  # Add lines for the other versions also
  #geom_line(data=secroData, aes(x = x, y = HeapUsed / 1024 / 1024), show.legend = TRUE, color="Orange") +
  #geom_line(data=cjrdtData, aes(x = x, y = HeapUsed / 1024 / 1024), show.legend = TRUE, color="Purple") +
  
  # Add 95% confidence intervals
  geom_errorbar(aes(ymin = df$lowerCI / 1024 / 1024, ymax = df$upperCI / 1024 / 1024), alpha=0.8) +
  # Add confidence bands for the other versions also
  #geom_ribbon(aes(ymin = secroData$lowerCI / 1024 / 1024, ymax = secroData$upperCI / 1024 / 1024), alpha=0.4) +
  #geom_ribbon(aes(ymin = cjrdtData$lowerCI / 1024 / 1024, ymax = cjrdtData$upperCI / 1024 / 1024), alpha=0.4) +
  #geom_errorbar(aes(ymin = df$lowerCI / 1024 / 1024, ymax = df$upperCI / 1024 / 1024),, colour="black", width=.1) +
  #geom_line(data = deleteData, color = "red") +
  # Set interval of 10 for x-axis
  scale_x_continuous(breaks = seq(0, 1000, by = 100)) +
  #labs(title = "Heap usage of the tree text editor") +
  #labs(caption = "Error bars represent the 95% CI.\nCommit and forced garbage collect after every operation.") +
  labs(x = "# Executed Operations") +
  labs(y = "Heap Usage in MB") +
  # Color the background based on the operations that are performed
  geom_rect(aes(NULL, NULL, xmin = xStart, xmax = xEnd+0.99999, fill = Operation),
           ymin = 0, ymax = 100, data = operationIntervals, inherit.aes = FALSE) + # ymin = yrng[1], ymax = yrng[2]
  scale_fill_manual(values = alpha(c("red", "green"), .3), breaks=c("insert", "delete"), labels=c(" insert  ", " delete")) +
  scale_linetype_manual("Data", values=c("solid", "dashed"), breaks=c("data", "trend"), labels=c(" Measured  ", " Regression line")) +
  theme_bw() +
  theme(legend.position = "top") +
  #theme(legend.background = element_rect(size=.5, linetype="solid", colour="black")) + # Add boxes around legends
  theme(legend.text=element_text(size=15),
        legend.title=element_blank(), #element_text(size=17),
        axis.text=element_text(size=15),
        axis.text.y=element_text(size=15),
        axis.text.x=element_text(size=15),
        axis.title=element_text(size=15,face="bold")
        ) +
  guides(color=guide_legend(override.aes=list(fill=NA), order=1)) +
  guides(linetype=guide_legend(override.aes=list(fill=NA, color="black"), order=0))
