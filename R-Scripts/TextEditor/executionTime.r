library(dplyr)

# Plots the execution time of various versions
# in function of the number of executed operations (i.e. operation history size).

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

# Load the data
# Remove the first row (x = 0), because it is not meaningful for the execution time
# Retain only rows with a sample size >= 20, because we computed confidence intervals
# based on those samples. And if we had less than 20 samples, it might not be a good 
# approximation of the normal distribution (i.e. central limit theorem does not hold).
cjrdtData  <- read.table("../../data/TextEditor/Time-Original/CjRDT/summary.txt",      header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()

svData     <- read.table("../../data/TextEditor/Time-Original/SECRO/summary.txt",      header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc1Data   <- read.table("../../data/TextEditor/Time-Original/SECRO_C1/summary.txt",   header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc10Data  <- read.table("../../data/TextEditor/Time-Original/SECRO_C10/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc20Data  <- read.table("../../data/TextEditor/Time-Original/SECRO_C20/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc50Data  <- read.table("../../data/TextEditor/Time-Original/SECRO_C50/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc100Data <- read.table("../../data/TextEditor/Time-Original/SECRO_C100/summary.txt", header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()

treeData    <- read.table("../../data/TextEditor/Time-Original/TREE/summary.txt",     header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
tree1Data   <- read.table("../../data/TextEditor/Time-Original/TREE_C1/summary.txt",   header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
tree10Data  <- read.table("../../data/TextEditor/Time-Original/TREE_C10/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
tree20Data  <- read.table("../../data/TextEditor/Time-Original/TREE_C20/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
tree50Data  <- read.table("../../data/TextEditor/Time-Original/TREE_C50/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
tree100Data <- read.table("../../data/TextEditor/Time-Original/TREE_C100/summary.txt", header=TRUE) %>% filter(x != 0 & SampleSize >= 20) %>% arrange(x) %>% as.data.frame()

# Add 'Version' column
cjrdtData$Version  <- rep(c("CjRDT"), times=nrow(cjrdtData))

svData$Version     <- rep(c("SECRO"), times=nrow(svData))    
svc1Data$Version   <- rep(c("SECRO"), times=nrow(svc1Data)) 
svc10Data$Version  <- rep(c("SECRO"), times=nrow(svc10Data))
svc20Data$Version  <- rep(c("SECRO"), times=nrow(svc20Data))
svc50Data$Version  <- rep(c("SECRO"), times=nrow(svc50Data))
svc100Data$Version <- rep(c("SECRO"), times=nrow(svc100Data))

treeData$Version    <- rep(c("TREE"), times=nrow(treeData))    
tree1Data$Version   <- rep(c("TREE"), times=nrow(tree1Data)) 
tree10Data$Version  <- rep(c("TREE"), times=nrow(tree10Data))
tree20Data$Version  <- rep(c("TREE"), times=nrow(tree20Data))
tree50Data$Version  <- rep(c("TREE"), times=nrow(tree50Data))
tree100Data$Version <- rep(c("TREE"), times=nrow(tree100Data))

# Add 'CommitInterval' column
cjrdtData$CommitInterval  <- rep(c("/"),    times=nrow(cjrdtData))

svData$CommitInterval     <- rep(c("0"),    times=nrow(svData))
svc1Data$CommitInterval   <- rep(c("1"),    times=nrow(svc1Data))
svc10Data$CommitInterval  <- rep(c("10"),   times=nrow(svc10Data))
svc20Data$CommitInterval  <- rep(c("20"),   times=nrow(svc20Data))
svc50Data$CommitInterval  <- rep(c("50"),   times=nrow(svc50Data))
svc100Data$CommitInterval <- rep(c("100"),  times=nrow(svc100Data))

treeData$CommitInterval    <- rep(c("0"),    times=nrow(treeData))
tree1Data$CommitInterval   <- rep(c("1"),    times=nrow(tree1Data))
tree10Data$CommitInterval  <- rep(c("10"),   times=nrow(tree10Data))
tree20Data$CommitInterval  <- rep(c("20"),   times=nrow(tree20Data))
tree50Data$CommitInterval  <- rep(c("50"),   times=nrow(tree50Data))
tree100Data$CommitInterval <- rep(c("100"),  times=nrow(tree100Data))

# Only plot measurements at a certain interval
interval <- 10
idxs  <- c(2, seq(from = 10, to = 1000, by = interval))
idxs2 <- c(2, seq(from = 10, to = 500, by = interval))

#cjrdtData   <- cjrdtData[idxs,] %>% na.omit() %>% as.data.frame()
svData     <- svData[idxs2,]    %>% na.omit() %>% as.data.frame()
#svc1Data   <- svc1Data[idxs,]   %>% na.omit() %>% as.data.frame()
#svc10Data  <- svc10Data[idxs,]
#svc20Data  <- svc20Data[idxs,]
#svc50Data  <- svc50Data[idxs,]
#svc100Data <- svc100Data[idxs,]
#svc100Data <- svc100Data %>% filter(x %% 10 == 0)

treeData <- treeData[idxs2,] %>% na.omit() %>% as.data.frame()

# Combine the dataframes of all versions
data <- do.call("rbind", list(svData, svc1Data, svc100Data)) #list(cjrdtData, svData, svc1Data, svc100Data, treeData, tree1Data, tree100Data))

#
# PROBABLY BETTER TO make 2 plots
# 1 plot containing the CjRDT version and the SECRO and TREE versions with commit interval 1
# to get a nice overview of the linearity of the three and their differences.
#
# And another plot with the SECRO & TREE without commits
# and with commit intervals of 50 AND/OR 100 for instance.
#

library(ggplot2)

# Plot different versions in a different colour
# and different commit intervals in a different line type.
ggplot(data, aes(x = x, y = MedianTime, color=CommitInterval)) + # linetype=Version
  geom_line() +
  scale_linetype_manual(values=c("dotted", "dashed", "solid"))+
  geom_ribbon(aes(ymin = data$lowerCI, ymax = data$upperCI), alpha=0.2) +
  labs(title="Execution time") +
  labs(x="# Operations") + 
  labs(y="Time in milliseconds")
