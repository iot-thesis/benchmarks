library(dplyr)

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

#runData <- read.table("../src/out/benchmarks/mem/SV/run.txt", header=TRUE)
#gcData <- read.table("../src/out/benchmarks/mem/SV/gc.txt", header=TRUE)

###### LOAD DATA ########
#runData <- read.table("../src/out/TT/runs.txt", header=TRUE)
cjrdt_Data <- read.table("../data/TextEditor/memory/CjRDT/run.txt", header=TRUE)
secro_Data <- read.table("../data/TextEditor/memory/SECRO/run.txt", header=TRUE)
tree_Data <- read.table("../data/TextEditor/memory/TREE/run.txt", header=TRUE)
#runData$x <- runData$x + 1 # Because "x" started from 0, but we want operations to be numbered starting from 1
colnames(cjrdt_Data)[ncol(cjrdt_Data)] <- "Operation"
colnames(secro_Data)[ncol(secro_Data)] <- "Operation"
colnames(secro_C100_Data)[ncol(secro_C100_Data)] <- "Operation"

cjrdt_Data$Version <- rep(c("JSON CRDT"), times=nrow(cjrdt_Data))
secro_Data$Version <- rep(c("SECRO"), times=nrow(secro_Data))
tree_Data$Version <- rep(c("TREE"), times=nrow(tree_Data))
runData <- c(cjrdt_Data, secro_Data, tree_Data)

# Group measurements by "x" value.
# Then, for each "x" value compute the mean,
# standard deviation and 95% confidence intervals.
processedData <- runData %>%
                    group_by(x, Version) %>%
                    select(x, HeapUsed, Operation)  %>%
                    mutate(n = n(),
                           Operation = Operation,
                           MHeapUsed = mean(HeapUsed), 
                           stdev = sd(HeapUsed),
                           lowerCI = HeapUsed - (qnorm(0.975) * stdev / sqrt(n)),
                           upperCI = HeapUsed + (qnorm(0.975) * stdev / sqrt(n))) %>%
                    distinct(x, .keep_all=TRUE) %>%
                    filter(x != 0) %>%
                    arrange(x) %>%
                    as.data.frame()

#processedData <- processedData[2:nrow(processedData),]
#cjrdtData <- read.table("../src/out/TT/runsCJRDT.txt", header=TRUE)
#cjrdtData$x <- cjrdtData$x + 1
#colnames(cjrdtData)[ncol(cjrdtData)] <- "Operation"
#cjrdtData <- cjrdtData[2:nrow(cjrdtData),]
#
#svData <- read.table("../src/out/TT/runs.txt", header=TRUE)
#svData$x <- svData$x + 1
#colnames(svData)[ncol(svData)] <- "Operation"
#svData <- svData[2:nrow(svData),]
#
#df <- rbind(runData, cjrdtData)
#df <- rbind(df, svData)

#runData <- runData[2:nrow(runData),]
df <- processedData #runData
########################

# Add version to each row
#runData$Version <- rep(c("SECRO (Interval 1)"), times=nrow(runData))
#df$Version <- rep(c("SECRO (Interval 1)"), times=nrow(df))
#cjrdtData$Version <- rep(c("CjRDT"), times=nrow(cjrdtData))
#svData$Version <- rep(c("SECRO"), times=nrow(svData))

# Compute the intervals of the consecutive operations
# Based on: https://stackoverflow.com/questions/32529854/group-data-in-r-for-consecutive-rows
opData <- df[1:nrow(df),] # remove the first row which is the initial memory usage
opData$Op <- with(opData, ifelse(Operation == "insert", 1, 2))
print(opData)
operationIntervals <- opData %>%
  group_by(Operation, group_weight = cumsum(c(1, diff(Op) != 0))) %>%
  summarise(xStart = min(x), xEnd = max(x)) %>%
  arrange(xStart) # sort the groups in ascending order

#operationIntervals[1, "xStart"] = operationIntervals[1, "xStart"] - 5

# Take the inner join of the runs and the garbage collects
#gcs <- merge(runData, gcData, by.x = "x", by.y = "afterX")
#joined <- merge(x = runData, y = gcData, by.x = "x", by.y = "afterX", all.x = TRUE) # left outer join

library(ggplot2)

yrng <- range(df$HeapUsed / 1024 / 1024)

# Plot heap space usage
ggplot(df, aes(x = x, y = HeapUsed / 1024 / 1024), color=Version) + #, color = Version)) +
  geom_line(show.legend = TRUE) + #, color = "green") +
  # Add 95% confidence intervals
  geom_ribbon(aes(ymin = df$lowerCI / 1024 / 1024, ymax = df$upperCI / 1024 / 1024), alpha=0.4) +
  #geom_line(data = deleteData, color = "red") +
  # Set interval of 10 for x-axis
  scale_x_continuous(breaks = seq(0, 1000, by = 100)) +
  labs(title = "Heap Usage", subtitle = "For the collaborative text editor.") +
  labs(caption = "Bands represent the 95% CI.\nCommit and forced garbage collect after every operation.") +
  labs(x = "Operations") +
  labs(y = "Memory in MB") +
  # Color the background based on the operations that are performed
  geom_rect(aes(NULL, NULL, xmin = xStart, xmax = xEnd+0.99999, fill = Operation),
           ymin = 0, ymax = 100, data = operationIntervals, inherit.aes = FALSE) + # ymin = yrng[1], ymax = yrng[2]
  scale_fill_manual(values = alpha(c("red", "green"), .3))
  
  # Show the operation that was performed
  #geom_point(data = runData, mapping = aes(x = x, y = HeapUsed / 1024 / 1024, color = Result),
             #alpha=0.7, size=4)

# Add markers showing where garbage collects occured
#p + geom_point(data = gcs, mapping = aes(x = x, y = HeapUsed / 1024 / 1024, color = GC), 
             #  alpha=0.7, size=4, show.legend = FALSE)
  # Add a legend
  #+ scale_shape_manual(values = c("MINOR" = 1))
  #+ scale_color_manual(values = c("MINOR" = 'green', 'MAJOR' = 'red'))
