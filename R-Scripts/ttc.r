library(dplyr)

# Plots the time to consistency for different versions

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

data <- read.table("../data/TextEditor/TTC/ttc.txt", header=TRUE)

#data <- data %>% filter(Version != "CJRDT") %>% as.data.frame()

# Compute the mean, standard deviation and 95% CI
# for each combination of version and number of operations.
# based on: https://stackoverflow.com/questions/16367436/compute-mean-and-standard-deviation-by-group-for-multiple-variables-in-a-data-fr
ag <- aggregate(Time ~ Version + TotalOps + CommitInterval, data, function(x) c(mean = mean(x), sd = sd(x)))

###############
#d <- data %>%
#  group_by(x) %>%
#  select(x, HeapUsed, Operation)  %>%
#  mutate(n = n(),
#         Operation = Operation,
#         MHeapUsed = mean(HeapUsed), 
#         stdev = sd(HeapUsed),
#         lowerCI = HeapUsed - (qnorm(0.975) * stdev / sqrt(n)),
#         upperCI = HeapUsed + (qnorm(0.975) * stdev / sqrt(n))) %>%
#  distinct(x, .keep_all=TRUE) %>%
#  #filter(x != 0) %>%
#  arrange(x) %>%
#  as.data.frame()
##############

data$CommitInterval[data$Version == "CJRDT"] <- "NA"

test <- data %>%
  filter(CommitInterval == 50 | CommitInterval == "NA") %>%
  #filter(Version == "TREE") %>%
  #filter(Version == "CJRDT") %>%
  #filter(Time < 100000) %>%
  group_by(Version, TotalOps, CommitInterval) %>%
  mutate(N = n()) %>% #,
         #Mean = mean(Time),
         #Sd = sd(Time),
         #LowerCI = Mean - (qnorm(0.975) * Sd / sqrt(N)),
         #UpperCI = Mean + (qnorm(0.975) * Sd / sqrt(N))) %>%
  arrange(Version, TotalOps, CommitInterval, Time) %>%
  # Compute 95% confidence interval for median
  # According to: http://www.ucl.ac.uk/ich/short-courses-events/about-stats-courses/stats-rm/Chapter_8_Content/confidence_interval_single_median
  mutate(Mean = median(Time) * 1.0,
         LowerIdx = round((N/2) - ((1.96 * sqrt(N)) / 2)),
         UpperIdx = round(1 + (N/2) + ((1.96 * sqrt(N)) / 2)),
         LowerCI = nth(Time, nth(LowerIdx, 1)), #slice(round((N/2) - ((1.96 * sqrt(N)) / 2))),
         UpperCI = nth(Time, nth(UpperIdx, 1))) %>% #slice(round(1 + (N/2) + ((1.96 * sqrt(N)) / 2)))) %>%
  as.data.frame()
  


# Add some columns
ag$N       <- rep(c(0), times=nrow(ag)) # 4
ag$Mean    <- rep(c(0), times=nrow(ag))
ag$Sd      <- rep(c(0), times=nrow(ag))
ag$LowerCI <- rep(c(0), times=nrow(ag))
ag$UpperCI <- rep(c(0), times=nrow(ag))

for (i in 1:nrow(ag)) {
    row <- ag[i,]
    version <- row$Version
    totalOps <- row$TotalOps
    commitInterval <- row$CommitInterval
    stats <- row[,4]
    
    mean <- stats[,1]
    sd <- stats[,2]
    
    # Compute 95% CI
    n <- nrow(subset(data, TotalOps==totalOps & Version==version))
    error <- qnorm(0.975)*sd/sqrt(n)
    lowerCI <- mean - error
    upperCI <- mean + error
    
    ag[i, 4] = n
    ag[i, 5] = mean
    ag[i, 6] = sd
    ag[i, 7] = lowerCI
    ag[i, 8] = upperCI
}

library(ggplot2)

# Plot different versions in a different colour
# and different commit intervals in a different line type.
plotTTC <- function() {
  ag <- test
  ggplot(ag, aes(x = TotalOps, y = Mean / 1000, colour=Version)) +
    geom_line() +
    geom_ribbon(aes(ymin = ag$LowerCI/1000, ymax = ag$UpperCI/1000), alpha=0.2) +
    #geom_errorbar(aes(ymin = ag$LowerCI/1000, ymax = ag$UpperCI/1000)) +
    labs(title="Time To Consistency", subtitle="For the collaborative text editor") +
    labs(x="# Operations") + 
    labs(y="Time in seconds")
}

plotThroughput <- function() {
  # We know the average and standard deviation for the execution time.
  # We also know that throughput = Operations / Time and Operations is a constant.
  # Therefore, the average and standard deviation change by the same constant.
  # http://www.stat.wmich.edu/s216/book/node43.html
  ag <- test
  ggplot(ag, aes(x = TotalOps, y = TotalOps / (Mean / 1000), colour=Version:factor(CommitInterval))) +
    geom_line() +
    geom_errorbar(aes(ymin = TotalOps / (ag$LowerCI/1000), ymax = TotalOps / (ag$UpperCI/1000))) +
    #geom_ribbon(aes(ymin = TotalOps / (ag$LowerCI/1000), ymax = TotalOps / (ag$UpperCI/1000)), alpha=0.2) +
    labs(title="Throughput", subtitle="For the collaborative text editor") +
    labs(x="# Operations") + 
    labs(y="Insertions/sec")
}

#plotThroughput()
plotTTC()