#rm(list = ls())
#.rs.restartR()

library(dplyr)

# Plots the execution time of various versions
# in function of the number of executed operations (i.e. operation history size).

# Set working directory to this file's directory
this.dir <- dirname(parent.frame(2)$ofile)
setwd(this.dir)

# Load the data
# Remove the first row (x = 0), because it is not meaningful for the execution time
# Retain only rows with a sample size >= 20, because we computed confidence intervals
# based on those samples. And if we had less than 20 samples, it might not be a good 
# approximation of the normal distribution (i.e. central limit theorem does not hold).
cjrdtData  <- read.table("../../data/Tangent/time/CjRDT/summary.txt",      header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svData     <- read.table("../../data/Tangent/time/SECRO/summary.txt",      header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc1Data   <- read.table("../../data/Tangent/time/SECRO_C1/summary.txt",   header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc10Data  <- read.table("../../data/Tangent/time/SECRO_C10/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc20Data  <- read.table("../../data/Tangent/time/SECRO_C20/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc50Data  <- read.table("../../data/Tangent/time/SECRO_C50/summary.txt",  header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()
svc100Data <- read.table("../../data/Tangent/time/SECRO_C100/summary.txt", header=TRUE) %>% filter(x != 0 & SampleSize >= 30) %>% arrange(x) %>% as.data.frame()

# Add 'Version' column
cjrdtData$Version  <- rep(c("CjRDT"),      times=nrow(cjrdtData))  
svData$Version     <- rep(c("SECRO"),      times=nrow(svData))    
svc1Data$Version   <- rep(c("SECRO_C1"),   times=nrow(svc1Data)) 
svc10Data$Version  <- rep(c("SECRO_C10"),  times=nrow(svc10Data))
svc20Data$Version  <- rep(c("SECRO_C20"),  times=nrow(svc20Data))
svc50Data$Version  <- rep(c("SECRO_C50"),  times=nrow(svc50Data))
svc100Data$Version <- rep(c("SECRO_C100"), times=nrow(svc100Data))

# Add 'CommitInterval' column
#cjrdtData$CommitInterval  <- rep(c("/"),    times=nrow(cjrdtData))
#svData$CommitInterval     <- rep(c("0"),    times=nrow(svData))
#svc1Data$CommitInterval   <- rep(c("1"),    times=nrow(svc1Data))
#svc10Data$CommitInterval  <- rep(c("10"),   times=nrow(svc10Data))
#svc20Data$CommitInterval  <- rep(c("20"),   times=nrow(svc20Data))
#svc50Data$CommitInterval  <- rep(c("50"),   times=nrow(svc50Data))
#svc100Data$CommitInterval <- rep(c("100"),  times=nrow(svc100Data))

# Only plot measurements at a certain interval
interval   <- 10
cjrdtData  <- cjrdtData     %>% filter(x %% interval == 0) #%>% filter(!(x > 600 & x < 720))
svData     <- svData        %>% filter(x == 1 | x %% interval == 0)
svc1Data   <- svc1Data      %>% filter(x %% interval == 0)
#svc10Data  <- svc10Data     %>% filter(x %% interval == 0)
#svc20Data  <- svc20Data     %>% filter(x %% interval == 0)
#svc50Data  <- svc50Data     %>% filter(x %% interval == 0)
#svc100Data <- svc100Data    %>% filter(x %% interval == 0)


# Combine the dataframes of all versions
#data <- do.call("rbind", list(cjrdtData, svc1Data, svc50Data, svData))

# Combine dataframes for only the CjRDT and SECRO C1 versions
data <- do.call("rbind", list(cjrdtData, svc1Data))

library(ggplot2)

# Plot different versions in a different colour
# and different commit intervals in a different line type.
ggplot(data, aes(x = x, y = MedianTime, color=Version)) + #color=CommitInterval, linetype=Version)) +
  geom_line() +
  scale_linetype_manual(values=c("dotted", "solid"))+
  geom_ribbon(aes(ymin = data$lowerCI, ymax = data$upperCI), alpha=0.2) +
  #geom_errorbar(aes(ymin = data$lowerCI, ymax = data$upperCI)) +
  labs(title="Execution time for a constant time operation") +
  labs(x="# Operations") + 
  labs(y="Time in milliseconds") +
  theme_bw() +
  # Legen name + item names
  scale_color_discrete(name="Version",
                       breaks=c("CjRDT", "SECRO", "SECRO_C1", "SECRO_C50"), 
                       labels=c("JSON CRDT",  "SECRO", "SECRO C1", "SECRO C50")) +
  theme(legend.background = element_rect(size=.5, linetype="solid", colour="black")) # Add boxes around legends
  
