# Benchmarks

This repository contains all benchmarking material:

* Benchmarking code
  * [Execution Time](https://gitlab.com/iot-thesis/benchmarks/blob/master/src/TextEditorBenchmarks/ExecTime/TimeBenchmark.js)
  * [Memory Usage](https://gitlab.com/iot-thesis/benchmarks/tree/master/src/TextEditorBenchmarks/Memory)
  * [Time To Consistency / Throughput](https://gitlab.com/iot-thesis/benchmarks/tree/master/src/TextEditorBenchmarks/TimeToConsistency)
*  [Collected data](https://gitlab.com/iot-thesis/benchmarks/tree/master/data/TextEditor)
*  [R Scripts](https://gitlab.com/iot-thesis/benchmarks/tree/master/R-Scripts/TextEditor/finals)
*  [Plots](https://gitlab.com/iot-thesis/benchmarks/tree/master/plots/PaperPlots)